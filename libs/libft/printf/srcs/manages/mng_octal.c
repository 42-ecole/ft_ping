/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   mng_octal.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rfunk <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/01/12 12:07:37 by rfunk             #+#    #+#             */
/*   Updated: 2021/01/13 21:36:21 by rfunk            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

static void	get_size(t_format *f, uintmax_t oc, int *size)
{
	*size = (oc == 0) ? 1 : 0;
	if (f->keys & HASH && oc != 0)
		*size += 1;
	while (oc != 0)
	{
		oc /= 8;
		*size += 1;
	}
	if (f->keys & WIDTH && f->keys & ZERO)
		*size = (f->w > *size) ? f->w : *size;
	if (f->keys & PREC)
		*size = (f->prec > *size) ? f->prec : *size;
}

static void	get_octal(t_format *f, uintmax_t *oc)
{
	va_list copy;

	if (f->keys & DOLL)
	{
		va_copy(copy, f->lst);
		while (--f->argc > 0)
			va_arg(copy, void*);
	}
	if (f->keys & INTMAX || f->keys & LONG1)
		*oc = va_arg((f->keys & DOLL) ? copy : f->lst, uintmax_t);
	else if (f->keys & SHORT)
		*oc = (unsigned short)va_arg((f->keys & DOLL) ? copy : f->lst,
		unsigned int);
	else if (f->keys & SHORT1)
		*oc = (unsigned char)va_arg((f->keys & DOLL) ? copy : f->lst,
		unsigned int);
	else if (f->keys & LONG || *(f->pos) == 'O')
		*oc = va_arg((f->keys & DOLL) ? copy : f->lst, unsigned long int);
	else if (f->keys & SIZE_T)
		*oc = va_arg((f->keys & DOLL) ? copy : f->lst, size_t);
	else
		*oc = va_arg((f->keys & DOLL) ? copy : f->lst, unsigned int);
}

static void	print_octal(t_format *f, uintmax_t oc, int size)
{
	if (size > 0)
	{
		if (f->keys & PREC && f->prec == 0 && oc == 0)
		{
			if (f->w > 0)
				add_char(f, 32);
			if (f->keys & HASH)
				add_char(f, 48);
			return ;
		}
		print_octal(f, oc / 8, --size);
		add_char(f, oc % 8 + 48);
	}
}

void		mng_octal(t_format *f)
{
	int			size;
	uintmax_t	oc;

	get_octal(f, &oc);
	get_size(f, oc, &size);
	if (!(f->keys & ALIGN) && !(f->keys & ZERO))
		add_values(f, 32, f->w - size);
	print_octal(f, oc, size);
	if (f->keys & ALIGN && f->w > size)
		add_values(f, 32, f->w - size);
}
