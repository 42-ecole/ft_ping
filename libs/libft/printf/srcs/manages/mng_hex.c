/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   mng_hex.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rfunk <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/01/12 12:07:31 by rfunk             #+#    #+#             */
/*   Updated: 2021/01/13 21:36:16 by rfunk            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

static void	get_size(t_format *f, uintmax_t hex, int *size)
{
	*size = (hex == 0) ? 1 : 0;
	while (hex != 0)
	{
		hex /= 16;
		*size += 1;
	}
	if (f->keys & PREC)
		*size = (f->prec > *size) ? f->prec : *size;
	if (f->keys & HASH)
		*size += 2;
	if (f->keys & WIDTH && f->keys & ZERO)
		*size = (f->w > *size) ? f->w : *size;
}

static void	get_hex(t_format *f, uintmax_t *hex)
{
	va_list copy;

	if (f->keys & DOLL)
	{
		va_copy(copy, f->lst);
		while (--f->argc > 0)
			va_arg(copy, void*);
	}
	if (f->keys & INTMAX || f->keys & LONG1)
		*hex = va_arg((f->keys & DOLL) ? copy : f->lst, uintmax_t);
	else if (f->keys & SHORT)
		*hex = (unsigned short)va_arg((f->keys & DOLL) ? copy : f->lst,
		unsigned int);
	else if (f->keys & SHORT1)
		*hex = (unsigned char)va_arg((f->keys & DOLL) ? copy : f->lst,
		unsigned int);
	else if (f->keys & LONG || *(f->pos) == 'U')
		*hex = va_arg((f->keys & DOLL) ? copy : f->lst, unsigned long int);
	else if (f->keys & SIZE_T)
		*hex = va_arg((f->keys & DOLL) ? copy : f->lst, size_t);
	else
		*hex = va_arg((f->keys & DOLL) ? copy : f->lst, unsigned int);
}

static void	print_hex(t_format *f, uintmax_t hex, int size)
{
	char value;

	if (size > 0)
	{
		value = hex % 16;
		print_hex(f, hex / 16, --size);
		if (f->keys & PREC && f->prec == 0 && hex == 0)
		{
			if (f->w > 0)
			{
				if (f->keys & HASH)
					add_values(f, 32, 2);
				add_char(f, 32);
			}
			return ;
		}
		else if (*(f->pos) == 'x')
			add_char(f, (value > 9) ? value + 87 : value + 48);
		else if (*(f->pos) == 'X')
			add_char(f, (value > 9) ? value + 55 : value + 48);
	}
}

void		mng_hex(t_format *f)
{
	int			size;
	uintmax_t	hex;

	get_hex(f, &hex);
	get_size(f, hex, &size);
	if (!(f->keys & ALIGN) && !(f->keys & ZERO))
		add_values(f, 32, f->w - size);
	if (f->keys & HASH && hex != 0)
	{
		add_char(f, '0');
		add_char(f, (*(f->pos) == 'x') ? 'x' : 'X');
	}
	print_hex(f, hex, (f->keys & HASH) ? size - 2 : size);
	if (f->keys & ALIGN && f->w > size)
		add_values(f, 32, f->w - size);
}
