/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rfunk <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/01/12 12:05:25 by rfunk             #+#    #+#             */
/*   Updated: 2021/05/12 22:26:43 by rfunk            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

static void		clean_format(t_format *f)
{
	f->argc = 0;
	f->keys = 0;
	f->prec = 0;
	f->w = 0;
}

static bool		init_format(t_format *f)
{
	f->buf_size = 50;
	if (!(f->output = ft_strnew(f->buf_size)))
		return (0);
	f->argc = 0;
	f->keys = 0;
	f->prec = 0;
	f->w = 0;
	f->len = 0;
	return (1);
}

static bool		manage_text_format(t_format *f)
{
	if (compare(f, "{red}"))
		add_string(f, "\033[31m");
	else if (compare(f, "{green}"))
		add_string(f, "\033[32m");
	else if (compare(f, "{yellow}"))
		add_string(f, "\033[33m");
	else if (compare(f, "{blue}"))
		add_string(f, "\033[34m");
	else if (compare(f, "{magenta}"))
		add_string(f, "\033[35m");
	else if (compare(f, "{cyan}"))
		add_string(f, "\033[36m");
	else if (compare(f, "{bold}"))
		add_string(f, "\033[1m");
	else if (compare(f, "{italic}"))
		add_string(f, "\033[3m");
	else if (compare(f, "{underline}"))
		add_string(f, "\033[4m");
	else if (compare(f, "{eoc}"))
		add_string(f, "\033[0m");
	else
		return (0);
	return (1);
}

static void		manage_output(t_format *f)
{
	while (*(f->pos))
	{
		if (*(f->pos) == '{')
		{
			if (!manage_text_format(f))
				add_char(f, *(f->pos));
		}
		else if (*(f->pos) == '%')
		{
			mng_conversion(f);
			clean_format(f);
		}
		else
			add_char(f, *(f->pos));
		f->pos++;
	}
}

int				ft_printf(const char *format, ...)
{
	t_format f;

	if (format[0] == '%' && !format[1])
		return (0);
	if (!(init_format(&f)))
		return (-1);
	f.pos = (char*)format;
	va_start(f.lst, format);
	manage_output(&f);
	va_end(f.lst);
	if (f.output)
	{
		if (f.len)
			write(1, f.output, f.len);
		free(f.output);
		return (f.len);
	}
	return (-1);
}

int				ft_asprintf(char **dst, const char *format, ...)
{
	t_format f;

	if (format[0] == '%' && !format[1])
		return (0);
	if (!(init_format(&f)))
		return (-1);
	f.pos = (char*)format;
	va_start(f.lst, format);
	manage_output(&f);
	va_end(f.lst);
	if (f.output)
	{
		if (f.len)
			*dst = f.output;
		return (f.len);
	}
	return (-1);
}

char			*ft_asrprintf(const char *format, ...)
{
	t_format 	f;

	if (format[0] == '%' && !format[1])
		return (0);
	if (!(init_format(&f)))
		return (NULL);
	f.pos = (char*)format;
	va_start(f.lst, format);
	manage_output(&f);
	va_end(f.lst);
	if (f.output)
	{
		if (f.len)
			return (f.output);
		return (NULL);
	}
	return (NULL);
}
