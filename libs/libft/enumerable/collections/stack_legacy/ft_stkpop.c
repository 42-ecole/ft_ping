/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_stkpop.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rfunk <rfunk>                              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/26 18:22:35 by rfunk             #+#    #+#             */
/*   Updated: 2020/11/14 16:59:21 by rfunk            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "stack.h"
#include "stack_p.h"

/*
* Function for extracting elements of stack.
* \param t_stack** pointer to begin of source stack.
* \return Top element of the stack.
*/
t_object	*stk_popobj(struct s_istack *stack)
{
	t_object	*obj;

	if (stack->objs)
	{
		stack->count--;
		obj = (t_object *)stack->enumerable.current(stack);
		stack->enumerable.move_next(stack);
		stack->objs = stack->ptr;
		obj->next = NULL;
		return (obj);
	}
	return (NULL);
}

void		*stk_pop(struct s_istack *stack)
{
	t_object	*obj;

	if ((obj = stk_popobj(stack)))
		return ((void *)obj->content);
	return (NULL);
}
