/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_fstkreverse.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rfunk <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/11/16 12:50:50 by rfunk             #+#    #+#             */
/*   Updated: 2020/11/16 18:14:07 by rfunk            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "stack.h"
#include "stack_p.h"

void	fstk_reverse(t_istack *s, int start, int end)
{
	t_object	tmp;

	while (start < end)
	{
		tmp = s->objs[s->max_size - start - 1];
		s->objs[s->max_size - start - 1] = s->objs[s->max_size - end - 1];
		s->objs[s->max_size - end - 1] = tmp;
		start++;
		end--;
	}
}
