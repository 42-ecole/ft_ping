/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   stack_p.h                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rfunk <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/11/10 16:55:40 by rfunk             #+#    #+#             */
/*   Updated: 2020/12/08 16:43:09 by rfunk            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
** This is private headers, which would be set in initialization of i_stack.
*/

#ifndef STACK_P_H
# define STACK_P_H

#include "i_object.h"
#include "enumerable.h"

void		fstk_push(struct s_istack *stack, void *data);
t_object	*fstk_popobj(t_istack *stack);
void		*fstk_pop(t_istack *stack);
void		*fstk_peek(t_istack *stack);
void		*fstk_indexof(struct s_istack *stack, int index);
void		fstk_reverse(t_istack *s, int start, int end);
void		fstk_shift(t_istack *s, int index);
bool	    ftsk_is_ascorder(t_istack *s);

t_object	*stk_popobj(struct s_istack *stack);
void		*stk_pop(struct s_istack *stack);
void		*stk_peek(struct s_istack *stack);
void		stk_pushobj(struct s_istack *stack, t_object *obj);
void		stk_push(struct s_istack *stack, void *obj);
void		*stk_indexof(struct s_istack *stack, int index);
void        stk_reverse(t_istack *stack, int start, int end);
void        stk_shift(t_istack *stack, int index);
bool	    stk_is_ascorder(t_istack *s);

#endif