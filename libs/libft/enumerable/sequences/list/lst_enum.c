/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lst_enum.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rfunk <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/11/19 23:49:50 by rfunk             #+#    #+#             */
/*   Updated: 2020/12/09 12:18:14 by rfunk            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "list.h"
#include "list_p.h"

bool	lst_movenext(void *sequence)
{
	t_list *lst;

	lst = (t_list *)sequence;
    if (lst->index + 1 < lst->count)
    {
        lst->index++;
	    return (true);
    }
	return (false);
}

bool	lst_hasnext(void *sequence)
{
	t_list *lst;

	lst = (t_list *)sequence;
    if (lst->index + 1 < lst->capacity)
	    return (true);
	return (false);
}

void	lst_reset(void *sequence)
{
	t_list *lst;

	lst = (t_list *)sequence;
	lst->index = 0;
}

void	*lst_current(void *sequence)
{
	t_list *lst;

	lst = (t_list *)sequence;
	return (lst->index_of(lst, lst->index));
}

size_t	lst_get_content_size(void *sequence)
{
	t_list *lst;

	lst = (t_list *)sequence;
	return (lst->content_size);
}

size_t	lst_get_count(void *sequence)
{
	t_list *lst;

	lst = (t_list *)sequence;
	return (lst->count);
}
