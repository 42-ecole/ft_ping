/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   list.h                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rfunk <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/11/20 00:38:02 by rfunk             #+#    #+#             */
/*   Updated: 2021/05/12 17:38:40 by rfunk            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LIST_H
# define LIST_H

/*
	Array List - strongly typed list, which allow data to be stored in predefined type.
	If you don't know size of your type use List instead of. 
*/

# include <stdlib.h>
# include <stdbool.h>
# include "../../../memory/memory.h"
# include "../../enumerable.h"
# include "../../i_object.h"

#define DEFAULT_CAPACITY 16

/*
 From the input array determines a pointer to an object with own size by index and casts to the type.
 Use this macros if you want iterate in array with own content size. 

 Can be used in collections as interface.
*/
#define GET_OBJ(arr, idx, type, size) (((type*)((char(*)[size])(arr))[idx]))

#define CAST_TYPE(type, obj) ((type *)(obj))

#define ADD_OBJ(arr, type, data) \
		({*CAST_TYPE(type, arr->enumerable.current(arr)) = data; \
			arr->index++; \
			arr->count++;})

typedef struct	s_list
{
	size_t					index;
	size_t					count;
	size_t					capacity;
	size_t					content_size;
	struct s_ienumerable	enumerable;
	void					*(*index_of)(struct s_list *, int);
	void					(*reverse)(struct s_list *, int, int);
	void					(*shift)(struct s_list *, int);
	bool					(*is_sorted)(struct s_list *);
	void					(*add)(struct s_list *, void *);
	void					(*add_f)(struct s_list *, void *);
	void					(*add_size)(struct s_list *, void *, size_t);
	void					(*add_range)(struct s_list *, struct s_list *);
	void					*(*remove)(struct s_list *, int);
	void					*objs;
}				t_list;

t_list		*new_list(size_t capacity, size_t content_size);
t_list		*new_sublist(t_list *src, int index, int len);

//rework
bool	lst_is_ascorder(t_list *lst);
bool	lst_is_descorder(t_list *lst);

t_list		*ft_select(struct s_ienumerable ienumerable, void *seq,
			bool (*selector)(void *, void *), void *sel_data);

void	lst_sort(void *sequence, int low, int high,
			bool (*comparator)(void *, void*));
bool	comparator_int(void *el, void *el2);

#endif