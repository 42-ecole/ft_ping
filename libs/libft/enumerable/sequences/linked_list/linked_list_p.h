/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   linked_list_p.h                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rfunk <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/11/30 17:18:19 by rfunk             #+#    #+#             */
/*   Updated: 2020/12/08 10:43:07 by rfunk            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LINKED_LIST_P_H
# define LINKED_LIST_P_H

# include "linked_list.h"

bool	llst_hasnext(void *sequence);
bool	llst_movenext(void *sequence);
void	llst_reset(void *sequence);
void	*llst_current(void *sequence);
size_t	llst_get_content_size(void *sequence);
size_t	llst_get_count(void *sequence);
void	llst_addobj(struct s_llist *lst, t_object *obj);
void	llst_add(void *lst, void *data);
#endif