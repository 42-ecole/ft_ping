/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_foreach.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rfunk <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/11/13 17:47:47 by rfunk             #+#    #+#             */
/*   Updated: 2021/05/15 14:33:54 by rfunk            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "enumerable.h"
#include "i_object.h"
#include <stddef.h>
#include <stdio.h>
#include "list.h"
#include "linked_list.h"

void	foreach(struct s_ienumerable ienumerable, void *sequence,
		void (*func)(void *, void *), void *func_data)
{
	void	*obj;
	
	if (!ienumerable.get_count(sequence))
		return ;
	ienumerable.reset(sequence);
	while ((obj = ienumerable.current(sequence)))
	{
		func(obj,func_data);
		if (!ienumerable.move_next(sequence))
			break;
	}
	ienumerable.reset(sequence);
}

t_list		*ft_select(struct s_ienumerable ienumerable, void *seq,
			bool (*selector)(void *, void *), void *sel_data)
{
	t_list		*selected;

	ienumerable.reset(seq);
	selected = new_list(ienumerable.get_count(seq),
						ienumerable.get_content_size(seq));
	while (ienumerable.current(seq))
	{
		if (selector(ienumerable.current(seq), sel_data))
			selected->add(selected, ienumerable.current(seq));
		if (!ienumerable.move_next(seq))
			break;
	}
	return (selected);
}

void	add_range(struct s_ienumerable idst, void *dst,
			struct s_ienumerable isrc, void *src,
			int low, int len)
{
	void	*obj;
	// t_list	*srcl;

	// srcl = (t_list *)src;
	idst.reset(dst);
	isrc.reset(src);
	while (low)
	{
		if (!isrc.move_next(src))
			break;
		low--;
	}
	while ((obj = isrc.current(src)) && len)
	{
		ft_memcpy(idst.current(dst), obj, idst.get_content_size(dst));
		if (!isrc.move_next(src))
			break;
		if (!idst.move_next(dst))
			break;
		len--;
	}
	idst.reset(dst);
	isrc.reset(src);
}

