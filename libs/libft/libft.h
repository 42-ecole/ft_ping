/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   libft.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rfunk <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/26 18:27:54 by rfunk             #+#    #+#             */
/*   Updated: 2021/05/13 20:07:51 by rfunk            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LIBFT_H
# define LIBFT_H

# include <string.h>
# include <stdlib.h>
# include <stdbool.h>
# include <unistd.h>
# include <pthread.h>
# include <stdarg.h>
# include "enumerable/sequences/list/list.h"
# include "enumerable/sequences/linked_list/linked_list.h"
# include "enumerable/collections/stack/stack.h"
# include "enumerable/collections/hash_table/hash_table.h"
# include "memory/memory.h"

# define BUFF_SIZE 22
# define PATH_MAX 4096
# define INT_MAX 2147483647
# define INT_MIN -2147483648

#ifdef __linux__
	#define uint32_t u_int32_t
#else
	# include "ft_printf.h"
#endif

void			ft_freeargs(int count, ...);
bool			ft_is_hex(char *str);
int				ft_isnum(char *str);
int				ft_rm_char(char *dest, char c);
void			ft_bzero(void *s, size_t n);
size_t			ft_strlen(const char *s);
int				ft_strlen2(const char *str);
size_t			ft_strnlen(const char *s, size_t len);
size_t			ft_numlen(int nbr);
int				ft_wchar_len(wchar_t c);
int				ft_wstr_len(wchar_t *wstr);
char			*ft_lsttostr(t_list *lst);
t_list			*ft_strstolst(char **strs, size_t count);
char			*ft_strdup(const char *s);
char			*ft_strdupbfrn(const char *str);
char			*ft_strcpy(char *dst, const char *src);
char			*ft_strncpy(char *dst, const char *src, size_t len);
char			*ft_strcat(char *s1, const char *s2);
char			*ft_strncat(char *s1, const char *s2, size_t n);
size_t			ft_strlcat(char *dst, const char *src, size_t size);
char			*ft_strchr(const char *s, int c);
char			*ft_strrchr(const char *s, int c);
int				ft_strcmp(const char *s1, const char *s2);
int				ft_strncmp(const char *s1, const char *s2, size_t n);
char			*ft_strstr(const char *haystack, const char *needle);
size_t			ft_strstri(const char *haystack, const char *needle);
char			*ft_strnstr(const char *haystack,
				const char *needle, size_t len);
int				ft_atoi(const char *str);
double			ft_atof(const char *str);
int64_t			ft_atol(const char *str);
int				ft_isalpha(int c);
int				ft_isdigit(int c);
int				ft_isalnum(int c);
int				ft_isascii(int c);
int				ft_isprint(int c);
int				ft_str_is_num(const char *str);
int				ft_toupper(int c);
int				ft_tolower(int c);
char			*ft_strnew(size_t size);
void			ft_strdel(char **as);
bool			ft_strisdupl(char *str);
char			*ft_strdel_chars(const char *str, int count, ...);
void			ft_strclr(char *s);
void			ft_striter(char *s, void (*f)(char *));
void			ft_striteri(char *s, void (*f)(unsigned int, char *));
char			*ft_strmap(char const *s, char (*f)(char));
char			*ft_strmapi(char const *s, char (*f)(unsigned int, char));
int				ft_strequ(char const *s1, char const *s2);
int				ft_strnequ(char const *s1, char const *s2, size_t n);
char			*ft_strsub(char const *s, unsigned int start, size_t len);
char			*ft_strjoin(char const *s1, char const *s2);
char			*ft_joinargs(int count, ...);
char			*ft_strtrim(char const *s);
char			**ft_strsplit(char const *s, char c);
char			**ft_strsplit_withcount(char const *s, char c, int *count);
char			*ft_itoa(int n);
char			*ft_utoa_base(uint32_t n, int base);
void			ft_putchar(char c);
void			ft_putstr(char const *s);
void			ft_putendl(char const *s);
void			ft_putnbr(int n);
void			ft_putchar_fd(char c, int fd);
void			ft_putstr_fd(char const *s, int fd);
void			ft_putendl_fd(char const *s, int fd);
void			ft_putnbr_fd(int n, int fd);
size_t			ft_strnlen(const char *s, size_t len);
int				ft_chratstr(const char *s, int c);
void			ft_putstrtab(char const **tab);
double			*ft_strsplittodouble(char const *s, char c);
void			ft_swap(int *x, int *y);
int				ft_strint(char *src, int c);
char			*ft_strjoinfree(char *s1, char *s2);
int				ft_atoi_base(const char *str, int base);
int				set_bit(const int value, const int pos);
int				unset_bit(const int value, const int pos);
int				get_bit(const int value, const int pos);
void			ft_set_in_range(float value, float min, float max);
bool			ft_is_in_range(float value, float min, float max);
int				get_next_line(const int fd, char **line);
char			*ft_curdir(void);
unsigned short	checksum(unsigned short *ptr, int nbytes);
double			timeval_diff(struct timeval *tv_1, struct timeval *tv_2);

#endif
