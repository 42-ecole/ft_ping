/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_is_hex.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mdooley <mdooley@student.21-school.ru>     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/10/11 19:06:57 by mdooley           #+#    #+#             */
/*   Updated: 2020/10/12 19:35:08 by mdooley          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

bool			ft_is_hex(char *str)
{
	size_t	i;

	i = 0;
	while (str[i])
	{
		if (!(ft_isdigit(str[i]) || (str[i] >= 'A' && str[i] <= 'F')
								|| (str[i] >= 'a' && str[i] <= 'f')))
			return (false);
		i++;
	}
	return (true);
}
