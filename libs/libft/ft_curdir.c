/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_curdir.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rfunk <rfunk@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/10/11 15:24:20 by rfunk             #+#    #+#             */
/*   Updated: 2020/10/11 15:25:10 by rfunk            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_curdir(void)
{
	char	*path;
	char	buf[PATH_MAX];
	size_t	len;

	if (getcwd(buf, sizeof(buf)) != NULL)
	{
		len = ft_strlen(buf);
		path = ft_strnew(len);
		ft_memcpy(path, buf, len);
		return (path);
	}
	return (NULL);
}
