/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rfunk <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/05/04 13:56:57 by rfunk             #+#    #+#             */
/*   Updated: 2021/05/10 13:16:57 by rfunk            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_PRINTF_H
# define FT_PRINTF_H

# include <unistd.h>
# include <stdarg.h>
# include <wchar.h>
# include <stdlib.h>
# include <stdio.h>
# include <stdbool.h>


# define ZERO (1 << 0)
# define PLUS (1 << 1)
# define MINUS (1 << 2)
# define SPACE (1 << 3)
# define ALIGN (1 << 4)
# define HASH (1 << 5)
# define DOLL (1 << 6)
# define WIDTH (1 << 7)
# define PREC (1 << 8)
# define LONG (1 << 9)
# define LONG1 (1 << 10)
# define LONG2 (1 << 11)
# define SHORT (1 << 12)
# define SHORT1 (1 << 13)
# define INTMAX (1 << 14)
# define SIZE_T (1 << 15)
# define PREFFIX (1 << 16)

# define DIGIT(x) (x > 47 && x < 58) ? 1 : 0
# define ABS(x) (x < 0) ? -x : x

/*
**	Expanded char for national languages.
*/
# define WCHAR21 ((c >> 6) | 0xC0)
# define WCHAR22 ((c & 0x3F) | 0x80)
# define WCHAR31 ((c >> 12) | 0xE0)
# define WCHAR32 (((c >> 6) & 0x3F) | 0x80)
# define WCHAR33 ((c & 0X3F) | 0x80)
# define WCHAR41 ((c >> 18) | 0xF0)
# define WCHAR42 (((c >> 12) & 0x3F) | 0x80)
# define WCHAR43 (((c >> 6) & 0x3F) | 0x80)
# define WCHAR44 ((c & 0x3F) | 0x80)

typedef struct	s_format
{
	va_list	lst;
	size_t	buf_size;
	char	*pos;
	char	*output;
	short	argc;
	int		keys;
	int		prec;
	int		w;
	int		len;
}				t_format;

int				ft_printf(const char *format, ...);
int				ft_asprintf(char **dst, const char *format, ...);
char			*ft_asrprintf(const char *format, ...);

void			mng_conversion(t_format *f);
void			mng_char(t_format *f);
void			mng_string(t_format *f);
void			mng_address(t_format *f);
void			mng_decimal(t_format *f);
void			mng_unsigned(t_format *f);
void			mng_octal(t_format *f);
void			mng_hex(t_format *f);
void			mng_double(t_format *f);
void			mng_bits(t_format *f);

/*
**	Libft functions.
*/
void			ft_strdel(char **str);
char			*ft_strnew(size_t size);
char			*ft_strjoin(const char *s1, const char *s2);
char			*ft_strdup(const char *str);
void			add_char(t_format *f, char c);
void			add_wchar(t_format *f, wchar_t c);
void			add_sign(t_format *f);
void			add_string(t_format *f, char *str);
bool			compare(t_format *f, char *text_format);
void			ld_to_ascii(t_format *f, long double value,
					size_t size, char *str);
void			printf_parse_flags(t_format *f);
void			add_values(t_format *f, char value, int size);
bool			double_output(t_format *f);

#endif
