/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strdel_chars.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rfunk <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/05/10 14:20:51 by rfunk             #+#    #+#             */
/*   Updated: 2021/05/10 15:10:34 by rfunk            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strdel_char(const char *str, char *c)
{
	char	*clear_str;
	char	*begin;
	char	*old_str;
	size_t	j;

	j = 0;
	old_str = (char *)str;
	clear_str = ft_strnew(ft_strlen(str));
	begin = clear_str;
	while (old_str && j < ft_strlen(old_str) - ft_strlen(c) + 1)
	{
		if (!ft_strncmp(old_str + j, c, ft_strlen(c)))
		{
			if (!j)
				old_str++;
			else
			{
				ft_strncpy(clear_str, old_str, j);
				old_str += j;
				clear_str += j;
				j = 0;
			}
		}
		else
			j++;
	}
	ft_strncpy(clear_str, old_str, j);
	return (begin);
}

char	*ft_strdel_chars(const char *str, int count, ...)
{
	va_list	ap;
	char	*clear_str;
	char	*s;
	int		i;

	i = 0;
	va_start(ap, count);
	clear_str = ft_strdup(str);
	va_start(ap, count);
	while (i++ < count)
	{
		s = va_arg(ap, char*);
		clear_str = ft_replace(clear_str, ft_strdel_char(clear_str, s));
	}
	va_end(ap);
	return (clear_str);
}