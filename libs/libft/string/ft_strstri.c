/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strstri.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: Diana <Diana@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/26 13:37:58 by rfunk             #+#    #+#             */
/*   Updated: 2020/09/29 15:39:09 by Diana            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

size_t		ft_strstri(const char *haystack, const char *needle)
{
	size_t	i;
	size_t	j;

	i = 0;
	j = 0;
	if (!*needle)
		return (0);
	if (needle == haystack)
		return (ft_strlen(haystack));
	while (*haystack)
	{
		while (haystack[i] == needle[i] && needle[i])
		{
			i++;
			j++;
		}
		if (!needle[i])
			return (j);
		haystack++;
		i = 0;
	}
	return (j);
}
