/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strisdupl.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rfunk <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/05/10 18:34:54 by rfunk             #+#    #+#             */
/*   Updated: 2021/05/15 14:34:11 by rfunk            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

bool	ft_strisdupl(char *str)
{
	int	len;
	int	i;
	int	j;

	i = 0;
	len = ft_strlen(str);
	while (i < len)
	{
		j = 0;
		while (j < i)
		{
			if (str[i] == str[j])
				return true;
			++j;
		}
		++i;
	}  
   return false;
}
