/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lsttostr.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rfunk <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/05/12 20:41:18 by rfunk             #+#    #+#             */
/*   Updated: 2021/05/12 21:22:17 by rfunk            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_lsttostr(t_list *lst)
{
	char	*str;
	char	c;
	size_t	i;

	i = 0;
	str = ft_strnew(lst->count);
	while (i < lst->count)
	{
		c = *CAST_TYPE(char, lst->index_of(lst, i));
		if (ft_isprint(c))
			str[i] = c;
		++i;
	}
	return (str);
}
