/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   mem_manager.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: Diana <Diana@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/10/02 20:09:57 by Diana             #+#    #+#             */
/*   Updated: 2020/10/10 16:30:03 by Diana            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "malloc_gc.h"
#include "libft.h"

void		ft_lstdelel(t_list **lst, void *ptr)
{
	t_list		*tmp;
	void		*data;

	while (*lst)
	{
		data = (*lst)->content;
		if (data == ptr)
		{
			tmp = (*lst)->next;
			free(*lst);
			*lst = tmp;
			break ;
		}
		(*lst) = (*lst)->next;
	}
}

void		content_free(void *content, size_t size)
{
	ft_memset(content, 0, size);
	free(content);
}

void		memory_manager(void *ptr, bool action)
{
	static t_list	*collector = NULL;
	t_list			*el;

	if (ptr == NULL)
	{
		ft_lstdel(&collector, content_free);
		if (!collector)
			printf("gc is null\n");
	}
	if (action == M_ADD)
	{
		el = ft_lstnew(ptr, sizeof(void *));
		if (!collector)
			collector = el;
		else
			ft_lstadd(&collector, el);
	}
	else if (action == M_REMOVE)
	{
		ft_lstdelel(&collector, content_free);
	}
}

void		free_gc(void *ptr)
{
	memory_manager(ptr, M_REMOVE);
}

void		*malloc_gc(size_t size)
{
	void	*ptr;

	if (!(ptr = malloc(size)))
		return (NULL);
	memory_manager(ptr, M_ADD);
	return (ptr);
}
