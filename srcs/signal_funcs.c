/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sighdl.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rfunk <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/15 12:17:21 by rfunk             #+#    #+#             */
/*   Updated: 2021/05/13 20:11:37 by rfunk            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ping.h"

extern t_env	g_env;

void		signal_packet(int sig)
{
	(void)sig;
	send_packet();
	alarm(g_env.opt_delay);
}

void		signal_info(int sig)
{
	(void)sig;

	gettimeofday(&(g_env.tv_end), NULL);

	printf("\n--- %s ping statistics ---\n", g_env.tgt);
	printf("%d packets transmitted, ", g_env.n_transmitted);
	printf("%d received, ", g_env.n_received);
	if (g_env.n_errors != 0)
		printf("+%d errors, ", g_env.n_errors);
	printf("%d%% packet loss, ", 100 - ((g_env.n_received * 100) / g_env.n_transmitted));
	printf("time %.fms\n", timeval_diff(&(g_env.tv_start), &(g_env.tv_end)));
	if (g_env.n_received != 0)
		printf("rtt min/avg/max/stddev = %.3f/%.3f/%.3f/%.3f ms\n", g_env.rtt_min, g_env.rtt_sum / g_env.n_received, g_env.rtt_max, g_env.rtt_max - g_env.rtt_min);

	del_env_instance();
	exit(0);
}
