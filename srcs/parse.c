/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parse_args.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rfunk <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/08 19:24:55 by rfunk             #+#    #+#             */
/*   Updated: 2021/05/13 19:30:46 by rfunk            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ping.h"

extern t_env	g_env;

int			parse_args(int argc, char **argv)
{
	int			i;
	char		*p;

	if (argc < 2)
		return (printf("Usage: ./ft_ping [-fhqv] [-t ttl] [-i interval (sec)] target.\n"));
	i = 1;
	while (i < argc)
	{
		p = argv[i];
		if (!ft_strcmp(p, "-i"))
		{
			if (g_env.opt_flood == 1)
				return (printf("ft_ping: -i and -f are incompatible flags.\n"));
			else if (!ft_str_is_num(argv[i + 1]) || atoi(argv[i + 1]) < 0)
				return (printf("ft_ping: Invalid argument.\n"));
			else
				g_env.opt_delay = atoi(argv[i++ + 1]);
		}
		else if (!ft_strcmp(p, "-t"))
		{
			if (ft_str_is_num(argv[i + 1]) && atoi(argv[i + 1]) > 0)
				g_env.opt_ttl = atoi(argv[i++ + 1]);
			else
				return (printf("ft_ping: Invalid argument.\n"));
		}
		else if (*p == '-')
		{
			p++;
			while (*p != '\0')
			{
				if (*p == 'h')
					return (printf("Usage: ./ft_ping [-fhqv] [-t ttl] [-i interval (sec)] target.\n"));
				else if (*p == 'v')
					g_env.opt_verbose = 1;
				else if (*p == 'q')
					g_env.opt_quiet = 1;
				else if (*p == 'f')
				{
					if (g_env.opt_delay == -1)
						g_env.opt_flood = 1;
					else
						return (printf("ft_ping: -i and -f are incompatible flags.\n"));
				}
				else
					return (printf("ft_ping: Unknown option.\n"));
				p++;
			}
		}
		else
		{
			if (ft_str_is_num(argv[i]))
				return (printf("ft_ping: Invalid argument.\n"));
			if (g_env.opt_delay == -1)
				g_env.opt_delay = 1;
			g_env.tgt = argv[i];
			return (0);
		}
		i++;
	}
	return (printf("Usage: ./ft_ping [-fhqv] [-t ttl] [-i interval (sec)] target.\n"));
}
