/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rfunk <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/08 17:52:07 by rfunk             #+#    #+#             */
/*   Updated: 2021/05/13 19:47:00 by rfunk            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ping.h"

t_env	g_env;

int		main(int argc, char **argv)
{
	set_env_instance();
	if (parse_args(argc, argv) == -1)
		return (del_env_instance());
	if (resolve_host())
		return (del_env_instance());
	if ((g_env.sock = socket(AF_INET, SOCK_RAW, IPPROTO_ICMP)) == -1)
		return (del_env_instance());
	setsockopt(g_env.sock, SOL_SOCKET, SO_REUSEADDR, &(g_env.opt_reuseaddr), sizeof(g_env.opt_reuseaddr));
	setsockopt(g_env.sock, IPPROTO_IP, IP_TTL, &(g_env.opt_ttl), sizeof(g_env.opt_ttl));
	gettimeofday(&(g_env.tv_start), NULL);
	printf("PING %s (%s)\n", g_env.tgt, g_env.sdst);
	signal(SIGINT, signal_info);
	if (g_env.opt_delay != 0 && g_env.opt_flood != 1)
	{
		signal(SIGALRM, signal_packet);
		alarm(g_env.opt_delay);
	}
	send_packet();
	while (1)
	{
		read_packet();
		if (g_env.opt_delay == 0 || g_env.opt_flood == 1)
			send_packet();
	}
	return (del_env_instance());
}
