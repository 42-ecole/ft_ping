/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   resolve_host.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rfunk <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/08 19:33:17 by rfunk             #+#    #+#             */
/*   Updated: 2021/05/13 19:31:00 by rfunk            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ping.h"

extern t_env	g_env;

void	hints_addrinfo(struct addrinfo *info)
{
	info->ai_flags = 0;
	info->ai_family = AF_INET;
	info->ai_socktype = 0;
	info->ai_protocol = IPPROTO_ICMP;
	info->ai_addrlen = 0;
	info->ai_addr = NULL;
	info->ai_canonname = NULL;
	info->ai_next = NULL;
}

int		resolve_host(void)
{
	struct addrinfo *res;
	struct addrinfo addr_info;
	struct in_addr	addr;
	int				ret;

	ret = -1;
	if (!g_env.tgt)
		return (-1);
	hints_addrinfo(&addr_info);
	if (!getaddrinfo(g_env.tgt, NULL, &addr_info, &res))
	{
		g_env.dst = (struct sockaddr_in *)malloc(sizeof(struct sockaddr_in));
		if (g_env.dst)
		{
			ft_memcpy(g_env.dst, res->ai_addr, sizeof(struct sockaddr_in));
			addr = (g_env.dst)->sin_addr;
			inet_ntop(AF_INET, &(addr.s_addr), (char *)&(g_env.sdst), INET_ADDRSTRLEN + 1);
			ret = 0;
		}
		freeaddrinfo(res);
	}
	if (ret == -1)
		printf("ft_ping: Cannot resolve %s: Unknown host\n", g_env.tgt);
	return (ret);
}
