/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   packet_manage.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rfunk <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/15 15:43:09 by rfunk             #+#    #+#             */
/*   Updated: 2021/05/13 20:11:37 by rfunk            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ping.h"
#include "libft.h"

extern t_env	g_env;

void		set_base_msghdr(struct msghdr *msg)
{
	struct iovec		iov;
	char				buf[100];

	ft_memset(&iov, 0, sizeof(iov));
	ft_memset(msg, 0, sizeof(msg));
	iov.iov_base = buf;
	iov.iov_len = sizeof(buf);
	msg->msg_name = NULL;
	msg->msg_namelen = 0;
	msg->msg_iov = &iov;
	msg->msg_iovlen = 1;
	msg->msg_control = NULL;
	msg->msg_controllen = 0;
	msg->msg_flags = 0;
}

void		compute_result_info(double tv_delta)
{
		g_env.rtt_sum += tv_delta;
	if (tv_delta > g_env.rtt_max)
		g_env.rtt_max = tv_delta;
	if (g_env.rtt_min == 0)
		g_env.rtt_min = tv_delta;
	else if (tv_delta < g_env.rtt_min)
		g_env.rtt_min = tv_delta;
	g_env.n_received += 1;
}


void		read_packet(void)
{
	char 				*ptr;
	struct ip			*ip;
	struct icmp			*icmp;
	struct msghdr		msg;
	struct timeval		*tv_out;
	struct timeval		tv_in;
	double				tv_delta;
	int 				bytes_c;

	set_base_msghdr(&msg);
	if ((bytes_c = recvmsg(g_env.sock, &msg, 0)) > 0)
	{
		ptr = (char *)msg.msg_iov->iov_base;
		ip = (struct ip *)ptr;
		icmp = (struct icmp *)(ptr + (ip->ip_hl * 4));
		gettimeofday(&tv_in, NULL);
		tv_out = (struct timeval *)(icmp + 1);
		tv_delta = timeval_diff(tv_out, &tv_in);
		if (icmp->icmp_type != 8)
		{
			if (g_env.opt_quiet == 0)
			{
				if (icmp->icmp_type != 0)
				{
					if (g_env.opt_verbose == 1)
						printf("From %s: icmp_type=%d icmp_code=%d\n",
							g_env.sdst, icmp->icmp_type, icmp->icmp_code);
				}
				else
					printf("%d bytes from %s: icmp_seq=%d ttl=%d time=%.4f ms\n",
							bytes_c, g_env.sdst, icmp->icmp_seq, ip->ip_ttl, tv_delta);
			}
			if (icmp->icmp_type == 0)
				compute_result_info(tv_delta);
			else
				g_env.n_errors += 1;
		}
	}
}

void		set_icmp(char *raw_packet, size_t payload_size)
{
	struct icmp 		*icmp;

	icmp = (struct icmp *)raw_packet;
	icmp->icmp_type = 8;
	icmp->icmp_code = 0;
	icmp->icmp_cksum = 0;
	icmp->icmp_id = g_env.id;
	icmp->icmp_seq = g_env.seq;
	icmp->icmp_cksum = checksum((unsigned short *)icmp,
						sizeof(struct icmp) + payload_size);
}

char		*new_packet(size_t *packet_size)
{
	char				*raw_packet;
	size_t				payload_size;
	struct timeval		tv_out;

	payload_size = sizeof(struct timeval);
	*packet_size = sizeof(struct icmp) + payload_size;
	if (!(raw_packet = (char *)malloc(*packet_size)))
	{
		del_env_instance();
		exit(1);
	}
	ft_memset(raw_packet, 0, *packet_size);
	gettimeofday(&tv_out, NULL);
	ft_memcpy(raw_packet + sizeof(struct icmp), &tv_out, sizeof(struct timeval));
	set_icmp(raw_packet, payload_size);
	return (raw_packet);
}

void		send_packet(void)
{
	char				*raw_packet;
	size_t				packet_size;
	struct sockaddr_in	sock_in;

	sock_in.sin_family = AF_INET;
	sock_in.sin_addr.s_addr = g_env.dst->sin_addr.s_addr;
	ft_memset(&(sock_in.sin_zero), 0, sizeof(sock_in.sin_zero));
	raw_packet = new_packet(&packet_size);
	if (sendto(g_env.sock, raw_packet, packet_size, 0,
			(struct sockaddr *)&sock_in, sizeof(sock_in)) > 0)
	{
		g_env.seq += 1;
		g_env.n_transmitted += 1;
	}
	free(raw_packet);
}
