/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_ping.h                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rfunk <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/08 17:54:02 by rfunk             #+#    #+#             */
/*   Updated: 2021/05/13 20:08:29 by rfunk            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_PING_H
# define FT_PING_H

# include <stdio.h>
# include <stdlib.h>
# include <unistd.h>
# include <string.h>
# include <signal.h>
# include <ctype.h>
# include <sys/types.h>
# include <sys/socket.h>
# include <netdb.h>
// # include <netinet/in.h>
// # include <netinet/in_systm.h>
// # include <netinet/ip.h>
# include <netinet/ip_icmp.h>
# include <arpa/inet.h>
# include <sys/time.h>
# include "libft.h"

typedef struct	s_env
{
	int				sock;
	int				id;
	int				seq;
	char			*tgt;
	struct			sockaddr_in *dst;
	char			sdst[INET_ADDRSTRLEN + 1];
	int 			flg;
	int 			n_transmitted;
	int 			n_received;
	int 			n_errors;
	double 			rtt_min;
	double 			rtt_max;
	double 			rtt_sum;
	struct timeval	tv_start;
	struct timeval	tv_end;
	int				opt_reuseaddr;
	int				opt_verbose;
	int 			opt_delay;
	int 			opt_ttl;
	int 			opt_quiet;
	int 			opt_flood;
}				t_env;

void			set_env_instance(void);
int 			del_env_instance(void);
int				resolve_host(void);
int 			parse_args(int argc, char **argv);
void 			send_packet(void);
void 			read_packet(void);
void			signal_packet(int sig);
void			signal_info(int sig);

#endif
