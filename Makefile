

NAME := ft_ping
CC := gcc
CC_DEBUG := gcc -g
FLAGS = -Wall -Wextra -Werror
DEL := rm -rf

#-----------SOURCES---------------#
SRCS_PATH := ./srcs
SRC	= 	main.c \
		env.c \
		packet_manage.c \
		signal_funcs.c \
		parse.c \
		resolve_host.c
SRC_DIR := $(addprefix $(SRCS_PATH)/, $(SRC))

OBJ := $(SRC_DIR:.c=.o)
#----------------------------------#

#----------LIBRARIES--------------#
LIBFTDIR := libs/libft
LIBFTINC := .
LIBFT := ft
FTMAKE := +make -sC $(LIBFTDIR)/$(LIBFTINC)

LIBS := -L $(LIBFTDIR) -l $(LIBFT)

INCLUDES := -I $(SRCS_PATH) \
			-I $(LIBFTDIR)/$(LIBFTINC)
#----------------------------------#

#-----------STYLES-----------------#
NONE = \033[0m
INVERT := \033[7m
GREEN := \033[32m
RED := \033[31m
SUCCESS := [$(GREEN)✓$(NONE)]
SUCCESS2 := [$(INVERT)$(GREEN)✓$(NONE)]
APPOK := $(INVERT)$(GREEN)Compiled ✓$(NONE)$(INVERT):$(NAME) has been successfully compiled.$(NONE)
APPDELETED := $(INVERT)$(RED)Removed ✓$(NONE)$(INVERT):$(NAME) has been successfully removed.$(NONE)
#----------------------------------#

all:
	@make -s -j4 $(NAME)

$(OBJ): %.o: %.c
	@echo -n $(NAME):' $@: '
	@$(CC) -c $(FLAGS) $(INCLUDES) $< -o $@
	@echo "$(SUCCESS)"

$(LIBFT):
	@$(FTMAKE)

$(NAME):  $(OBJ) $(LIBFT)
	@$(CC) $(OBJ) $(LIBS) -o $(NAME)
	@echo "$(APPOK)"

debug:
	$(CC_DEBUG) $(SRC_DIR) $(LIBS) $(INCLUDES) -g -o $(NAME)

clean:
	@$(DEL) $(OBJ)
	@$(FTMAKE) clean

fclean: clean
	@$(FTMAKE) fclean
	@$(DEL) $(NAME)
	@echo "$(APPDELETED)"

re: fclean all

.PHONY: all clean fclean re
